resource "aws_instance" "my-app-dev" {
  ami                         = data.aws_ami.my-lastest-ami.id
  instance_type               = var.instance_type
  subnet_id                   = aws_subnet.my_subnet.id
  vpc_security_group_ids      = [aws_security_group.my_sg.id]
  associate_public_ip_address = true
  key_name                    = aws_key_pair.my_keypair.key_name

  tags = {
    Name = "dev-server"
  }
}

# resource "aws_instance" "my-app-staging" {
#   ami                         = data.aws_ami.my-lastest-ami.id
#   instance_type               = var.instance_type
#   subnet_id                   = aws_subnet.my_subnet.id
#   vpc_security_group_ids      = [aws_security_group.my_sg.id]
#   associate_public_ip_address = true
#   key_name                    = aws_key_pair.my_keypair.key_name

#   tags = {
#     Name = "staging-server"
#   }
# }

# resource "aws_instance" "my-app-prod" {
#   ami                         = data.aws_ami.my-lastest-ami.id
#   instance_type               = var.instance_type
#   subnet_id                   = aws_subnet.my_subnet.id
#   vpc_security_group_ids      = [aws_security_group.my_sg.id]
#   associate_public_ip_address = true
#   key_name                    = aws_key_pair.my_keypair.key_name

#   tags = {
#     Name = "prod-server"
#   }
# }





# resource "null_resource" "configure_server" {
# triggers = {
#   trigger = aws_instance.my-app.public_ip
# }
#   provisioner "local-exec" {
#    working_dir = "/Users/eawangya/Desktop/learn-ansible/deploy-docker"
#    command = "ansible-playbook --inventory ${aws_instance.my-app.public_ip}, --private-key ${var.ssh_key_private} --user ec2-user myplaybook.yml"
#   }
# }