pipeline {
    agent any

    environment {
    // Define the image version based on the Jenkins build number
    IMAGE_VERSION = "${BUILD_NUMBER}"
    ANSIBLE_SERVER = "24.144.80.35"

    }

    stages {
        // stage('GitHub Checkout') {
        //     steps {
        //         git branch: 'main', credentialsId: 'gitlab-creds', url: 'https://gitlab.com/ernest.awangya/userauthentication.git'
        //     }
        // }

        // stage('Docker Build and Push') {
        //     steps {
        //         withCredentials([usernamePassword(credentialsId: 'docker_creds', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        //             sh 'echo $PASS | docker login -u $USER --password-stdin'
        //             sh 'docker build -t eawangya/userauthentication:userauthentication-latest .'
        //             sh 'docker push eawangya/userauthentication:userauthentication-latest'
        //         }
        //     }
        // }
        
        stage('Provision Server') {
            environment {
                AWS_ACCESS_KEY_ID = credentials('jenkins_aws_access_key_id')
                AWS_SECRET_ACCESS_KEY = credentials('jenkins_aws_secret_access_key')
            }
            steps {
                script {
                    dir('terraform') {
                        sh 'terraform init'
                        //sh 'terraform apply --auto-approve'
                        sh 'terraform destroy --auto-approve'
                        EC2_LINUX_IP = sh(
                            script: "terraform output ec2_public_ip",
                            returnStdout: true
                        ).trim()

                        INSTANCE_ID = sh(
                            script: "terraform output ec2_instance_id", // Update this with the correct output name
                            returnStdout: true
                        ).trim()

                        echo "EC2 Linux Instance IP: $EC2_LINUX_IP"
                        echo "EC2 Instance ID: $INSTANCE_ID"
                    }
                }
            }
        }
        
//         stage('Wait for EC2 Instance to be Running') {
//     steps {
//         script {
//             withAWS(region: 'us-east-1', credentials: 'awscli-creds') {
//                 def instanceStatus = ''
//                 def systemStatus = ''
//                 def maxRetries = 50
//                 def retryCount = 0

//                 while ((instanceStatus != 'running' || systemStatus != 'ok') && retryCount < maxRetries) {
//                     sleep 5 // Wait for 30 seconds before checking again

//                     def instanceStatusOutput = sh(
//                         script: "aws ec2 describe-instance-status --instance-ids $INSTANCE_ID --query 'InstanceStatuses[0].InstanceState.Name' --output text",
//                         returnStdout: true
//                     ).trim()
//                     instanceStatus = instanceStatusOutput ?: 'pending'

//                     def systemStatusOutput = sh(
//                         script: "aws ec2 describe-instance-status --instance-ids $INSTANCE_ID --query 'InstanceStatuses[0].SystemStatus.Status' --output text",
//                         returnStdout: true
//                     ).trim()
//                     systemStatus = systemStatusOutput ?: 'insufficient-data'

//                     echo "Instance status: $instanceStatus"
//                     echo "System status: $systemStatus"

//                     if (instanceStatus != 'running' || systemStatus != 'ok') {
//                         retryCount++
//                         echo "Retrying in 30 seconds..."
//                     }
//                 }

//                 if (instanceStatus == 'running' && systemStatus == 'ok') {
//                     echo "Instance and system status are satisfactory. Proceed with deployment."
//                 } else {
//                     error "Instance or system status did not pass the checks within the specified retries."
//                 }
//             }
//         }
//     }
// }

        stage("Copy files to Ansible server") {
            steps {
                script {
                    echo "Copying files to the Ansible control node"
                    
                    // Use sshagent to temporarily load the ansible-server-key credentials
                    sshagent(['ansible-server-key']) {
                        // Copy Ansible files to the Ansible server
                        sh "scp -o StrictHostKeyChecking=no ansible/* root@24.144.80.35:/root"
                        
                        // Use withCredentials to load the ec2-server-key credentials
                        withCredentials([sshUserPrivateKey(credentialsId: 'jenkins-sever-key-ansible', keyFileVariable: 'keyfile', usernameVariable: 'user')]){
                            // Copy the private key to the Ansible server
                            sh 'scp $keyfile root@24.144.80.35:/root/.ssh/id_rsa'
                        }
                    }
                }
            }
        }
stage("Execute ansible playbook on target server") {
            steps {
                script {
                    echo "Calling playbook to configure target environment"

                    // Use the sshagent step to execute SSH commands on the remote server
                    sshagent(['ansible-server-key']) {
                        def remote = [:]
                        remote.name = "ansible-server"
                        remote.host = "${ANSIBLE_SERVER}"
                        remote.allowAnyHosts = true

                        withCredentials([sshUserPrivateKey(credentialsId: 'ansible-server-key', keyFileVariable: 'keyfile', usernameVariable: 'user')]){
                            remote.user = 'ec2-user'
                            remote.identityFile = 'keyfile'

                            // Execute ansible-playbook command
                            sh "ssh -o StrictHostKeyChecking=no -i $keyfile root@$ANSIBLE_SERVER 'ansible-playbook myplaybook.yml'"

                            // Remove the temporary private key file
                            sh "ssh -o StrictHostKeyChecking=no -i $keyfile root@$ANSIBLE_SERVER 'rm -f ~/.ssh/id_rsa'"
                        }
                    }
                }
            }
        }
    }
 // start of slack Notification

    post {
        always {
            // Send Slack notification for build start
            slackSend channel: 'jenkins-notifications', color: 'warning', message: "Build started! Job: ${env.JOB_NAME} - Build: #${env.BUILD_NUMBER}"
        }
        success {
            // Send Slack notification for successful builds
            slackSend channel: 'jenkins-notifications', color: 'good', message: "Build successful! Job: ${env.JOB_NAME} - Build: #${env.BUILD_NUMBER}"
        }
        failure {
            // Send Slack notification for failed builds
            slackSend channel: 'jenkins-notifications', color: 'danger', message: "Build failed! Job: ${env.JOB_NAME} - Build: #${env.BUILD_NUMBER}"
        }        
    }
// End of Slack Notification

}
